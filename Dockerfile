FROM adosztal/network_automation
MAINTAINER Oussema Essafi (oussema.essafi@♥gmail.com)
RUN rm -r /etc/ansible/*
WORKDIR /etc/ansible
COPY . .
